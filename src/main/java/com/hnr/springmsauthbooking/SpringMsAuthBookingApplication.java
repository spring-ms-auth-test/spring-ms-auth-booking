package com.hnr.springmsauthbooking;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class SpringMsAuthBookingApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringMsAuthBookingApplication.class, args);
	}
}
