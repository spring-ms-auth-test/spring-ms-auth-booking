package com.hnr.springmsauthbooking.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(schema = "tbl_booking")
public class BookingEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String name;
    private int count;
}
